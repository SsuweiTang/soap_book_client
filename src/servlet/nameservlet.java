package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





import soap_book_service.BookServiceStub;
import soap_book_service.BookServiceStub.GetMyNameResponse;


/**
 * Servlet implementation class nameservlet
 */
public class nameservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");

	
	
		String output = "";

		BookServiceStub stub = new BookServiceStub();
		BookServiceStub.GetMyName bookRequest = new BookServiceStub.GetMyName();
		bookRequest.setName(name);
		GetMyNameResponse nameResponse = stub.getMyName(bookRequest);
		
		output=nameResponse.get_return();


		try {

		

		
			
			request.setAttribute("output", output);

			RequestDispatcher rd = request.getRequestDispatcher("book.jsp");
			rd.forward(request, response);

		} finally {
			out.close();
		}

	
	}

}
