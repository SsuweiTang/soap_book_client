package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import soap_book_service.BookServiceStub;
import soap_book_service.BookServiceStub.Book;
import soap_book_service.BookServiceStub.GetBookByIsbnResponse;

/**
 * Servlet implementation class bookservlet
 */
public class bookservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		String isbn = request.getParameter("isbn");

		String myisbn = "", mytitle = "", myauthor = "", myabstract = "";
		int myid = 0;
		String table_header = "";
	
		String output = "";

		if(isbn==null||isbn.toString().equals(""))
		{	
			output="the isbn is null";
		}
		else
		{
		BookServiceStub stub = new BookServiceStub();
		BookServiceStub.GetBookByIsbn bookRequest = new BookServiceStub.GetBookByIsbn();
		
		bookRequest.setIsbn(isbn);
		GetBookByIsbnResponse bookResponse = stub.getBookByIsbn(bookRequest);

		table_header = "<table bgcolor='#d9f0ff' border='1' style='width:70%'>"
				+ "	<tr>"
				+ "<td>id</td>"
				+ "<td>title</td>"
				+ "<td>isbn</td>"
				+ "<td>author</td>" + "<td>abstract</td>" + "</tr>";

		if (bookResponse.get_return() != null) {

			output += table_header;
			output += "<tr>";

			for (Book b : bookResponse.get_return()) {

//				myid = b.getId();
//				myisbn = b.getIsbn();
//				mytitle = b.getTitle();
//				myabstract = b.getMyabstract();
//				myauthor = b.getAuthor();
				
				output+="<td>"+b.getId()+"</td>";
				output+="<td>"+b.getTitle()+"</td>";
				output+="<td>"+b.getIsbn()+"</td>";
				output+="<td>"+b.getAuthor()+"</td>";
				output+="<td>"+b.getMyabstract()+"</td>";

			}
			output += "</tr>";

		}
		else
		{
			output="no book";
		}
		
		}

		try {

			// request.setAttribute("isbn", isbn);
			// request.setAttribute("myid", myid);
			// request.setAttribute("myisbn", myisbn);
			// request.setAttribute("mytitle", mytitle);
			// request.setAttribute("myauthor", myauthor);
			// request.setAttribute("myabstract", +myabstract);

			request.setAttribute("deter", "1");

			
			request.setAttribute("output", output);

			RequestDispatcher rd = request.getRequestDispatcher("book.jsp");
			rd.forward(request, response);

		} finally {
			out.close();
		}

	}

}
